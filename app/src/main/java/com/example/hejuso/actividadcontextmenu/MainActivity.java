package com.example.hejuso.actividadcontextmenu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Recogemos el ListView de la vista activity_main
        listView = findViewById(R.id.listadoPersonas);
        registerForContextMenu(listView);

        // Metemos en un array el listado de ficheros hecho desde strings.xml
        String[] arrayNombres = getResources().getStringArray(R.array.nombres);

        // Con el metodo asList de "Arrays" le cambiamos el tipo a un Listado String y nos devuelve un array
        List<String> listadoNombres = Arrays.asList(arrayNombres);


        // Le pasamos por una parte el contexto, por otra el tipo de listado (predeterminado de android), otro layout para el campo de texto y por último el listado de nombres
        ArrayAdapter<String> selectorNombres = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, listadoNombres);


        // Le asignamos el adaptador a la vista ListView
        listView.setAdapter(selectorNombres);


    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        super.onCreateContextMenu(menu, v, menuInfo);
        // Nos guardamos en una variable la palabra seleccionada
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        String palabraSeleccionada = ((TextView) info.targetView).getText().toString();

        // Asignamos el header
        menu.setHeaderTitle(palabraSeleccionada);

        // Creamos el menú flotante (Que por alguna razón en mi ejercicio no se visualiza flotando y con el fondo negro, no se
        // si se debe a lo que comentas en la teoría sobre la versión de Android
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contextual_menu, menu);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        // Por el id recibido al hacer click en el item diferenciamos que botón se ha pulsado y mostramos un TOAST

        switch (item.getItemId()) {
            case R.id.item_1:
                Toast.makeText(getApplicationContext(), "Mostrado", Toast.LENGTH_LONG).show();

                return true;
            case R.id.item_2:
                Toast.makeText(getApplicationContext(), "Eliminado", Toast.LENGTH_LONG).show();

                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

}

